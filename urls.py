from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.views.decorators.cache import cache_page
from django.views.generic.base import RedirectView
from django.views.static import serve

from tcof import views

urlpatterns = [
    url(r'home/$', cache_page(60 * 15)(views.home_method), name='home_url_name'),
    url(r'doc/$', cache_page(60 * 15)(views.doc_method), name='doc_url_name'),
    url(r'search/$', views.search_method, name='search_url_name'),
    url(r'browse/$', cache_page(60 * 15)(views.browse_method), name='browse_url_name'),
    url(r'ppi/$', views.ppi_method, name='ppi_url_name'),
    url(r'go/(?P<id>[A-Za-z0-9\-\:]+)$', cache_page(60 * 15)(views.go_method), name='go_url_name'),
    url(r'c6/(?P<idx>[A-Za-z0-9\-\:\_\.]+)$', cache_page(60 * 15)(views.c6_method), name='c6_url_name'),
    url(r'c7/(?P<idx>[A-Za-z0-9\-\:\_\.]+)$', cache_page(60 * 15)(views.c7_method), name='c7_url_name'),
    url(r'REACTOME/(?P<idx>[A-Za-z0-9\-\:\_\.]+)$', cache_page(60 * 15)(views.reactome_method), name='reactome_url_name'),
    url(r'KEGG/(?P<idx>[A-Za-z0-9\-\:\_\.]+)$', cache_page(60 * 15)(views.kegg_method), name='kegg_url_name'),
    url(r'gene/(?P<sym>[A-Za-z0-9\-]+)$', cache_page(60 * 15)(views.gene_method), name='gene_url_name'),
    url(r'disease/(?P<idx>[A-Za-z0-9\-\:\_\.]+)$', cache_page(60 * 15)(views.disease_method), name='disease_url_name'),
    
    # redirect non-matching urls to home/
    url(r'^.*$', RedirectView.as_view(url='home/', permanent=False), name='index'), 
    url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),
]


