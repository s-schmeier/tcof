from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.db.models import Q, F, Count
from django.conf import settings # to access settings varables
# link to static assets with url = static('x.jpg')
from django.contrib.staticfiles.templatetags.staticfiles import static

from itertools import chain

from .models import Entity, Uniprot, Go, tftf, tftcof, tfother, anno_c6, anno_c7, c7, c6, anno_kegg, anno_reactome, anno_disease, tcoftcof

import csv
import collections
import re
import os.path

_APP_LINK_PREFIX = '/tcof'

# Create your views here.
def disease_method(request, idx):
    context = {}
    content = request.GET.get('content', '0')
    if content == '0':
        url_dict = {'DisGeNET':'http://linkedlifedata.com/resource/umls/id/%s',
                    'MGI-OMIM':'http://www.omim.org/entry/%s'}
        context["idx"] = idx
        disease = anno_disease.objects.filter(idx=idx).values("term", "source").distinct()[0]
        
        if len(disease) == 0:
            context["error"] = 'Disease-id: "%s" not found.'%idx
            return render(request, "tcof/404.html", context)
        else:
            context["term"] = disease["term"]
            context["source"] = disease["source"]
            if disease["source"] == 'DisGeNET':
                species = 'human'
            else:
                species = 'mouse'
            context["species"] = species
            context["url"] =  url_dict[disease["source"]] %(idx)
            return render(request, "tcof/disease.html", context)
            
    else:
        species = request.GET.get('species', '.*')
        a = Entity.objects.filter(type = "TF",
                                  species__regex=r'^%s$' % (species),
                                  anno_disease__idx=idx).annotate(tcount=Count('tftcof_tf__entity2',
                                                                     distinct=True)).distinct()
        context['data'] = create_data(a)

        a = Entity.objects.filter(type__in = ["HC","1","2","3"],
                                  species__regex=r'^%s$' % (species),
                                  anno_disease__idx=idx).annotate(tcount=Count('tftcof_tcof__entity1', distinct=True)).distinct()

        context['data2'] = create_data(a)
        return render(request, "tcof/termtables.html", context)


def kegg_method(request, idx):
    context = {}
    content = request.GET.get('content', '0')
    if content == '0':
        res = re.search('mmu', idx)
        if res:
            species = 'mouse'
        else:
            species = 'human'
        context["species"] = species

        context["idx"] = idx

        path = anno_kegg.objects.filter(idx=idx).values("term").distinct()[0]
        if len(path) == 0:
            context["error"] = 'Pathway-id: "%s" not found.'%idx
            return render(request, "tcof/404.html", context)
        else:
            context["term"] = path["term"]
            return render(request, "tcof/kegg.html", context)
            
    else:
        species = request.GET.get('species', '.*')
        
        a = Entity.objects.filter(type = "TF",
                                  species__regex=r'^%s$' % (species),
                                  anno_kegg__idx=idx).annotate(tcount=Count('tftcof_tf__entity2',
                                                                     distinct=True)).distinct()
        context['data'] = create_data(a)

        a = Entity.objects.filter(type__in = ["HC","1","2","3"],
                                  species__regex=r'^%s$' % (species),
                                  anno_kegg__idx=idx).annotate(tcount=Count('tftcof_tcof__entity1', distinct=True)).distinct()

        context['data2'] = create_data(a)
        return render(request, "tcof/termtables.html", context)


def reactome_method(request, idx):
    context = {}
    content = request.GET.get('content', '0')
    if content == '0':
        context["idx"] = idx
        res = re.search('MMU', idx)
        if res:
            species = 'mouse'
        else:
            species = 'human'
        context["species"] = species

        path = anno_reactome.objects.filter(idx=idx).values("term").distinct()[0]
        if len(path) == 0:
            context["error"] = 'Pathway-id: "%s" not found.'%idx
            return render(request, "tcof/404.html", context)
        else:
            context["term"] = path["term"]
            return render(request, "tcof/reactome.html", context)

    else:
        species = request.GET.get('species', '.*')

        a = Entity.objects.filter(type = "TF",
                                  species__regex=r'^%s$' % (species),
                                  anno_reactome__idx=idx).annotate(tcount=Count('tftcof_tf__entity2',
                                                                     distinct=True)).distinct()
        context['data'] = create_data(a)

        a = Entity.objects.filter(type__in = ["HC","1","2","3"],
                                  species__regex=r'^%s$' % (species),
                                  anno_reactome__idx=idx).annotate(tcount=Count('tftcof_tcof__entity1', distinct=True)).distinct()

        context['data2'] = create_data(a)
        return render(request, "tcof/termtables.html", context)


def c6_method(request, idx):
    context = {}
    content = request.GET.get('content', '0')
    
    if content == '0':
        context["type"] = 'c6'
        context["idx"] = idx
        msigdb = c6.objects.filter(idx=idx).values("term").distinct()[0]
        if len(msigdb) == 0:
            context["error"] = 'MSigDB-id: "%s" not found.'%idx
            return render(request, "tcof/404.html", context)
        else:
            context["term"] = msigdb["term"]
            return render(request, "tcof/msigdb.html", context)
    else:
        species = request.GET.get('species', '.*')

        a = Entity.objects.filter(type = "TF",
                                  species__regex=r'^%s$' % (species),
                                  anno_c6__idx=idx).annotate(tcount=Count('tftcof_tf__entity2',
                                                                     distinct=True)).distinct()
        context['data'] = create_data(a)

        a = Entity.objects.filter(type__in = ["HC","1","2","3"],
                                  species__regex=r'^%s$' % (species),
                                  anno_c6__idx=idx).annotate(tcount=Count('tftcof_tcof__entity1', distinct=True)).distinct()
        context['data2'] = create_data(a)
        return render(request, "tcof/termtables.html", context)


def c7_method(request, idx):
    context = {}
    content = request.GET.get('content', '0')
    
    if content == '0':
        context["type"] = 'c7'
        context["idx"] = idx
        msigdb = c7.objects.filter(idx=idx).values("term").distinct()[0]
        if len(msigdb) == 0:
            context["error"] = 'MSigDB-id: "%s" not found.'%idx
            return render(request, "tcof/404.html", context)
        else:
            context["term"] = msigdb["term"]
            return render(request, "tcof/msigdb.html", context)
    else:
        species = request.GET.get('species', '.*')

        a = Entity.objects.filter(type = "TF",
                                  species__regex=r'^%s$' % (species),
                                  anno_c7__idx=idx).annotate(tcount=Count('tftcof_tf__entity2',
                                                                     distinct=True)).distinct()
        context['data'] = create_data(a)

        a = Entity.objects.filter(type__in = ["HC","1","2","3"],
                                  species__regex=r'^%s$' % (species),
                                  anno_c7__idx=idx).annotate(tcount=Count('tftcof_tcof__entity1', distinct=True)).distinct()
        context['data2'] = create_data(a)
        return render(request, "tcof/termtables.html", context)


def go_method(request, id):
    context = {}
    content = request.GET.get('content', '0')
    
    if content == '0':
        context["goid"] = id
        go = Go.objects.filter(goid=id).values("goterm").distinct()[0]
        if len(go) == 0:
            context["error"] = 'GO-term: "%s" not found.'%id
            return render(request, "tcof/404.html", context)
        else:
            context["goterm"] = go["goterm"]
            return render(request, "tcof/go.html", context)
    else:
        species = request.GET.get('species', '.*')
        # reverse lookup of goid!!!
        # https://docs.djangoproject.com/en/1.9/topics/db/queries/#following-relationships-backward
        a = Entity.objects.filter(type = "TF",
                                  species__regex=r'^%s$' % (species),
                                  go__goid=id).annotate(tcount=Count('tftcof_tf__entity2',
                                                                     distinct=True)).distinct()
        context['data'] = create_data(a)

        a = Entity.objects.filter(type__in = ["HC","1","2","3"],
                                  species__regex=r'^%s$' % (species),
                                  go__goid=id).annotate(tcount=Count('tftcof_tcof__entity1', distinct=True)).distinct()
        context['data2'] = create_data(a)

        return render(request, "tcof/termtables.html", context)


def home_method(request):
    context = {}
    content = request.GET.get('content', '0')
    if content == '0':
        return render(request, "tcof/home_base.html", context)
    else:
        aCountsHSA = Entity.objects.filter(species='human').values('type').annotate(tcount=Count('type'))
        aCountsMMU = Entity.objects.filter(species='mouse').values('type').annotate(tcount=Count('type'))
        dMMU = {}
        for d in aCountsMMU:
            dMMU[d['type']] = d['tcount']
        dHSA = {}
        for d in aCountsHSA:
            dHSA[d['type']] = d['tcount']

        context['hsa'] = dHSA
        context['mmu'] = dMMU
        return render(request, "tcof/home.html", context)


def doc_method(request):
    context = {}
    content = request.GET.get('content', '0')
    if content == '0':
        return render(request, "tcof/doc_base.html", context)
    
    aCountsHSA = Entity.objects.filter(species='human').values('type').annotate(tcount=Count('type'))
    aCountsMMU = Entity.objects.filter(species='mouse').values('type').annotate(tcount=Count('type'))
    dMMU = {}
    for d in aCountsMMU:
        dMMU[d['type']] = d['tcount']
    dHSA = {}
    for d in aCountsHSA:
        dHSA[d['type']] = d['tcount']
        
    iPPItftf_hsa = tftf.objects.filter(entity1__species="human").values('entity1','entity2').distinct().count()
    iPPItftf_mmu = tftf.objects.filter(entity1__species="mouse").values('entity1','entity2').distinct().count()
    iPPItftcof_hsa = tftcof.objects.filter(entity1__species="human").values('entity1','entity2').distinct().count()
    iPPItftcof_mmu = tftcof.objects.filter(entity1__species="mouse").values('entity1','entity2').distinct().count()
    iPPItfother_hsa = tfother.objects.filter(entity1__species="human").values('entity1','entity2').distinct().count()
    iPPItfother_mmu = tfother.objects.filter(entity1__species="mouse").values('entity1','entity2').distinct().count()
    dHSA['tftf'] = iPPItftf_hsa
    dHSA['tftcof'] = iPPItftcof_hsa
    dHSA['tfother'] = iPPItfother_hsa
    dMMU['tftf'] = iPPItftf_mmu
    dMMU['tftcof'] = iPPItftcof_mmu
    dMMU['tfother'] = iPPItfother_mmu
    
    context['hsa'] = dHSA
    context['mmu'] = dMMU
    return render(request, "tcof/doc.html", context)


def ppi_method(request):
    """ For downloading ppi data """
    context = {}
    species = request.GET.get('species', '.*')
    class_str = request.GET.get('class', '.*')
    
    if species not in ["mouse", "human", ".*"]:
        context["error"] = 'Species "%s" not known.'%species
        return render(request, "tcof/404.html", context)
    
    aPPI1 = tftf.objects.filter(entity1__species__regex=r'^%s$'%species).distinct().order_by('entity1__symbol').values_list('entity1__symbol', 'entity1__id', 'entity1__type','entity2__symbol','entity2__id', 'entity2__type', 'entity1__species','pmid', 'dbid','exp','interaction','db')

    aPPI2 = tftcof.objects.filter(Q(entity1__species__regex=r'^%s$'%species) & Q(entity2__type__regex=class_str)).distinct().order_by('entity1__symbol').values_list('entity1__symbol', 'entity1__id', 'entity1__type','entity2__symbol','entity2__id', 'entity2__type', 'entity1__species', 'pmid', 'dbid','exp','interaction','db')
   
    dnl = request.GET.get('dnl', '0')
    if dnl == '1':
        filename = request.GET.get('f','data.csv')
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s"' % filename
        writer = csv.writer(response)
        writer.writerow( ['Symbol1', 'GeneID1', 'Type1', 'Symbol1', 'GeneID1', 'Type1', 'Species', 'PMID', 'Source', 'Experiment', 'Interaction_type','DB'] )
        for t in list(aPPI1) + list(aPPI2):
            writer.writerow( list(t) )
        return response
    else:
        context["error"] = 'You see this page because the PPI url should only be used with dnl=1.'
        return render(request, "tcof/404.html", context)

    
def search_method(request):
    #MyClass.objects.filter(name__icontains=my_parameter)
    context = {}
    if request.method == 'GET':
        content = request.GET.get('content', '0')
        query = request.GET.get('q', None)
        query = query.strip()

        if content == '0':
            context["search_term"] = '+'.join([s.strip() for s in query.split(' ')])  # make urlsafe
            return render(request, "tcof/search_base.html", context)
        elif content == '1' and query:
            context["search_term"] = query
            # use "query" to look up things and store in "result"
            aE = Entity.objects.filter( Q(name__icontains=query) | \
                                        Q(symbol__icontains=query) | \
                                        Q(id__icontains=query) | \
                                        Q(alt__icontains=query) | \
                                        Q(uniprot__upid__icontains=query) | \
                                        Q(uniprot__upacc__icontains=query) \
                                        ).values_list('id', 'symbol', 'name','species','alt','type','uniprot__upid', 'uniprot__upacc').distinct()
                  
            d = {}
            for t in aE:
                if t[5] not in ["1", "2", "3", "HC","TF"]: # type
                    continue
                a = list(t)
                a[4] =  ', '.join(a[4].split(','))
                if a[5] != 'TF':
                    a[5] = 'TcoF:%s' %a[5]
                key = tuple(a[0:6])
                if key in d:
                    d[key].append((a[6],a[7]))
                else:
                    d[key] = [(a[6],a[7])]

            aE2 = []
            for k,v in d.items():
                aE2.append(list(k)+[v])
                    
            context["search_results"] = aE2
        else:
            context["search_term"] = ""
            context["search_results"] = []
            
        return render(request, 'tcof/search.html', context)
    
    # if not GET method return homepage
    else:
        context["error"] = 'GET method not used.'
        return render(request, "tcof/404.html", context)
        

def browse_method(request):
    context = {}
    # build minimal information for the browse_base
    
    url_type = request.GET.get('type', 'x')
    context["url_type"] = url_type

    content = request.GET.get('content', '0')
    if url_type == "anno":
        # we look at browsing annotaion terms
        anno_temp_list = [s.strip() for s in request.GET.get('atype', '0').split(':')]
        try:
            anno_type = anno_temp_list[0]
        except KeyError:
            context["error"] = 'No annotatation type given.'
            return render(request, "tcof/404.html", context)

        anno_type_sub = ''
        if len(anno_temp_list) > 1:
            anno_type_sub = anno_temp_list[1]

        context["annotype"] = anno_type
        context["annotypesub"] = anno_type_sub
        if content == '0':
            return render(request, "tcof/browse_anno_base.html", context)
    
    else:
        if url_type == "tf":
            title_str = 'transcription factors'
            class_str = ''
        else:
            title_str = 'transcription co-factors'
            class_str = request.GET.get('class', 'all') # if no class given use all

        context['class'] = class_str
        context['title'] = title_str
        
        dnl = request.GET.get('dnl', '0')
        context["dnl"] = dnl

        species = request.GET.get('species', '.*')
        if species not in ["mouse", "human", ".*"]:
            context["error"] = 'Species "%s" not known.'%species
            return render(request, "tcof/404.html", context)
        if species == '.*':
            context['species'] = 'all'
        else:
            context['species'] = species

        if content == '0':
            return render(request, "tcof/browse_base.html", context) 

        
    # if content is requested     
    if url_type == 'tf':
        a = Entity.objects.filter(species__regex=r'^%s$'%species, type="TF").annotate(tcount=Count('tftcof_tf__entity2', distinct=True)).distinct()
        context['data'] = create_data(a)

        # download?
        # TODO: fix, dnl not working with the content request.
        if dnl == '1':
            filename = request.GET.get('f','data.csv')
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="%s"' % filename
            writer = csv.writer(response)
            writer.writerow( ['Symbol', 'GeneID', 'Name', 'Species', 'Interactions_with_tcofs', 'Type'] )
            a = a.order_by('symbol')
            for e in a:
                writer.writerow( [e.symbol, e.id, e.name, e.species, e.tcount, e.type] )
            return response
        else:
            return render(request, "tcof/browse.html", context)
    elif url_type == 'tcof':
        if class_str not in ["1", "2", "3", "HC", "all"]:
            context["error"] = 'Class "%s" not known.'%class_str
            return render(request, "tcof/404.html", context)
        else:
            if class_str == "all":
                a = Entity.objects.filter(species__regex=r'^%s$'%species, type__in=["1", "2", "3", "HC"]).annotate(tcount=Count('tftcof_tcof__entity1', distinct=True)).distinct()
            else:
                a = Entity.objects.filter(species__regex=r'^%s$'%species, type=class_str).annotate(tcount=Count('tftcof_tcof__entity1', distinct=True)).distinct()                
            context['data'] = create_data(a)

            # download?
            # TODO: fix, dnl not working with the content request.
            if dnl == '1':
                filename = request.GET.get('f','data.csv')
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename="%s"' % filename
                writer = csv.writer(response)
                writer.writerow( ['Symbol', 'GeneID', 'Name', 'Species', 'Interactions_with_tfs', 'Type'] )
                a = a.order_by('symbol')
                for e in a:
                    writer.writerow( [e.symbol, e.id, e.name, e.species, e.tcount, e.type] )
                return response
            else:
                return render(request, "tcof/browse.html", context)

    elif url_type == "anno":
        # we look at browseing annotaion terms
        anno_temp_list = [s.strip() for s in request.GET.get('atype', '0').split(':')]
        try:
            anno_type = anno_temp_list[0]
        except KeyError:
            context["error"] = 'No annotatation type given.'
            return render(request, "tcof/404.html", context)
        
        anno_type_sub = ''
        if len(anno_temp_list) > 1:
            anno_type_sub = anno_temp_list[1]
            
        context["annotype"] = anno_type
        context["annotypesub"] = anno_type_sub
        
        species = request.GET.get('species', '.*')
        if anno_type == 'MSigDB':
            if anno_type_sub == 'c6':
                #a = anno_c6.objects.filter(~Q(entity__type = '-') & Q(entity__species__regex=r'^%s$' % (species))).select_related('entity').distinct()
                a = c6.objects.all()
            elif anno_type_sub == 'c7':
                #a = anno_c7.objects.filter(~Q(entity__type = '-') & Q(entity__species__regex=r'^%s$' % (species))).select_related('entity').distinct()
                a = c7.objects.all()
        
            results_list = []
            for anno in a: 
                idx = str(anno.idx)
                term = str(anno.term)
                a_num = [anno.count_TF, anno.count_HC, anno.count_1, anno.count_2, anno.count_3]
                a_temp = ['<a title="Open term in TcoF-DB" href="%s/%s/%s"><i class="fa fa-arrow-circle-right"></i></a>' % (_APP_LINK_PREFIX, str(anno_type_sub), idx), '<a title="Open term in TcoF-DB" href="%s/%s/%s">%s</a>' % (_APP_LINK_PREFIX, str(anno_type_sub), idx, term),
                      '<a title="Open at original source" href="http://www.broadinstitute.org/gsea/msigdb/cards/%s">%s</a>' % (idx, 'MSigDB')] + [str(i) for i in a_num]
                results_list.append(a_temp)    
                
        elif anno_type == 'GO':
            a = Go.objects.filter(Q(gotype = anno_type_sub) & ~Q(entity__type = '-') & Q(entity__species__regex=r'^%s$' % (species))).select_related('entity').distinct()
            dict_anno = {}
            for anno in a:
                idx = str(anno.goid)
                term = str(anno.goterm)
                # get entity type, e.g. TF or TcoF
                entity_type = str(anno.entity.type)

                tindex = (idx, term)
                if tindex not in dict_anno:
                    dict_anno[tindex] = {"TF":0, "HC":0, "1":0, "2":0, "3":0}
                dict_anno[tindex][entity_type] += 1

            results_list = []
            for t in dict_anno:
                a_num = [dict_anno[t]["TF"], dict_anno[t]["HC"], dict_anno[t]["1"], dict_anno[t]["2"], dict_anno[t]["3"]]
                if sum(a_num) == 0:
                     continue
                idx = t[0]
                term = t[1]
                a_temp = ['<a title="Open term in TcoF-DB" href="%s/go/%s"><i class="fa fa-arrow-circle-right"></i></a>' % (_APP_LINK_PREFIX, idx), '<a title="Open term in TcoF-DB" href="%s/go/%s">%s</a>' % (_APP_LINK_PREFIX, idx, term),
                          '<a title="Open at original source" href="http://amigo.geneontology.org/amigo/term/%s">%s</a>' % (idx, idx)] + [str(i) for i in a_num]

                results_list.append(a_temp)

        elif anno_type == 'Pathways':
            re_path = re.compile('mmu', re.IGNORECASE)

            if anno_type_sub == 'KEGG':
                a = anno_kegg.objects.filter(~Q(entity__type = '-') & Q(entity__species__regex=r'^%s$' % (species))).select_related('entity').distinct()
                link_base = '<a title="Open at original source" href="http://www.genome.jp/dbget-bin/www_bget?%s">%s</a>'
            elif anno_type_sub == 'REACTOME':
                a = anno_reactome.objects.filter(~Q(entity__type = '-') & Q(entity__species__regex=r'^%s$' % (species))).select_related('entity').distinct()
                link_base = '<a title="Open at original source" href="http://www.reactome.org/PathwayBrowser/#/%s">%s</a>'

            dict_anno = {}
            for anno in a:
                idx = str(anno.idx)
                term = str(anno.term)
                # get entity type, e.g. TF or TcoF
                entity_type = str(anno.entity.type)

                tindex = (idx, term)
                if tindex not in dict_anno:
                    dict_anno[tindex] = {"TF":0, "HC":0, "1":0, "2":0, "3":0}
                dict_anno[tindex][entity_type] += 1

            results_list = []
            for t in dict_anno:
                a_num = [dict_anno[t]["TF"], dict_anno[t]["HC"], dict_anno[t]["1"], dict_anno[t]["2"], dict_anno[t]["3"]]
                if sum(a_num) == 0:
                     continue
                idx = t[0]
                term = t[1]
                res = re_path.search(idx)
                if res:
                    species = 'mouse'
                else:
                    species = 'human'
                
                a_temp = ['<a title="Open term in TcoF-DB" href="%s/%s/%s"><i class="fa fa-arrow-circle-right"></i></a>'%(_APP_LINK_PREFIX, str(anno_type_sub), idx), '<a title="Open term in TcoF-DB" href="%s/%s/%s">%s (%s)</a>' % (_APP_LINK_PREFIX, str(anno_type_sub), idx, term, species),
                          link_base % (idx, idx)] + [str(i) for i in a_num]
                results_list.append(a_temp)    

        elif anno_type == 'Diseases':
            url_dict = {'DisGeNET':'<a title="Open externally" href="http://linkedlifedata.com/resource/umls/id/%s">%s</a>',
                        'MGI-OMIM':'<a title="Open at OMIM" href="http://www.omim.org/entry/%s">%s</a>'}

            a = anno_disease.objects.filter(~Q(entity__type = '-')).select_related('entity').distinct()
            dict_anno = {}
            for anno in a:
                idx = str(anno.idx)
                term = str(anno.term)
                # get entity type, e.g. TF or TcoF
                entity_type = str(anno.entity.type)
                source = str(anno.source)

                tindex = (idx, term, source)
                if tindex not in dict_anno:
                    dict_anno[tindex] = {"TF":0, "HC":0, "1":0, "2":0, "3":0}
                dict_anno[tindex][entity_type] += 1
                
            results_list = []
            for t in dict_anno:
                a_num = [dict_anno[t]["TF"], dict_anno[t]["HC"], dict_anno[t]["1"], dict_anno[t]["2"], dict_anno[t]["3"]]
                if sum(a_num) == 0:
                     continue
                idx = t[0]
                term = t[1]
                source = t[2]
                if source == 'MGI-OMIM':
                    species = 'mouse'
                else:
                    species = 'human'
                
                a_temp = ['<a title="Open term in TcoF-DB" href="%s/%s/%s"><i class="fa fa-arrow-circle-right"></i></a>'%(_APP_LINK_PREFIX, 'disease', idx),'<a title="Open term in TcoF-DB" href="%s/%s/%s">%s (%s)</a>' % (_APP_LINK_PREFIX, 'disease', idx, term, species),
                          url_dict[source] % (idx, source)] + [str(i) for i in a_num]
                results_list.append(a_temp)    
            
        context["data"] = results_list
        return render(request, "tcof/browse_anno.html", context)
    
    else:
        context["error"] = 'Type "%s" not known.'%url_type
        return render(request, "tcof/404.html", context)

    
def gene_method(request, sym):
    context = {}
    a = Entity.objects.filter(symbol__regex=r'^%s$'%sym) # exact match, kind of a hack as the __exact did not work
    # a = Entity.objects.filter(symbol__exact=sym) # exact match # not case sensitive?
    if len(a)>1:
        context["error"] = 'Query "%s" resulted in more then 1 entitiy.'%sym
        return render(request, "tcof/404.html", context)
    elif len(a)==0:
        context["error"] = 'Query "%s" resulted in 0 entities.'%sym
        return render(request, "tcof/404.html", context)
    else:
        obj = a[0]
        
    obj.alt = obj.alt.replace(',',', ')
    context["query"] = sym
    
    if obj.type == "TF":
        context["type"] = "TF"
    elif obj.type == "-":
        context["type"] = "Other"
    else:
        context["type"] = "TcoF"
        
    context['e'] = obj
    # base info grabed. show base html and load further content with ajax
    # we basically get one more DB query, as up to here is exectured twice per
    # gene. However, Therefore we gain a spinner and faster base.html load
    content = request.GET.get('content', '0')
    if content == '0':
        # ppi tab to show?
        ppi_tab = request.GET.get('ppi', 'inactive')
        context["ppi_tab"] = ppi_tab
        return render(request, "tcof/gene_base.html", context)
    else:
        # ppi tab to show?
        ppi_tab = request.GET.get('ppi', 'inactive')
        context["ppi_tab"] = ppi_tab

        # uniprot
        aUP = Uniprot.objects.filter(entity=obj.id).distinct().order_by('kind')
        context["up"] = aUP

        # evidence
        if context["type"] == "TF":
            context["go"] = None
        else:
            # experimental term = **, non-exp term: *, other random go-terms = -
            #aGOexp = Go.objects.filter(entity=obj.id, kind='**']).distinct().order_by('gotype', 'goterm')
            #aGOnonexp = Go.objects.filter(entity=obj.id, kind='*']).distinct().order_by('gotype','goterm')

            # using 1 query
            if context["type"] == "TcoF":
                aGO = Go.objects.filter(entity=obj.id, kind__in=['**', '*']).distinct().order_by('gotype', 'goterm')
            else:
                aGO = Go.objects.filter(entity=obj.id).distinct().order_by('gotype', 'goterm')

            dGO = {'exp':[], 'nonexp':[], 'nonexp_general':[]}
            dEXPGO = {}
            for objGO in aGO:
                objGO.pmid = [s.strip() for s in objGO.pmid.split('|')]
                if objGO.kind == '**':
                    dEXPGO[objGO.goid] = None
                    dGO['exp'].append(objGO)

            for objGO in aGO:
                if objGO.kind == '*':
                    if objGO.goid not in dEXPGO:
                        dGO['nonexp'].append(objGO)
                elif objGO.kind == "-":
                    if objGO.goid not in dEXPGO:
                        dGO["nonexp_general"].append(objGO)
            context["go"] = dGO

        def adjust_data_tf(a):
            dPPI = collections.OrderedDict()
            for objPPI in a:
                t = (objPPI.entity1.symbol, objPPI.entity1.id)
                dPPI[t] = dPPI.get(t, []) + [objPPI]
            # sort according to symbol
            dPPI_FINAL = collections.OrderedDict(sorted(dPPI.items()))
            return dPPI_FINAL
        
        def adjust_data(a):
            dPPI = collections.OrderedDict()
            for objPPI in a:
                t = (objPPI.entity2.symbol, objPPI.entity2.id)
                dPPI[t] = dPPI.get(t, []) + [objPPI]
            # sort according to symbol
            dPPI_FINAL = collections.OrderedDict(sorted(dPPI.items()))
            return dPPI_FINAL

        def adjust_data2(a1, a2):
            dTemp = {'HC': collections.OrderedDict(),
                     '1': collections.OrderedDict(),
                     '2': collections.OrderedDict(),
                     '3': collections.OrderedDict()}
            for objPPI in a1:
                t = (objPPI.entity2.symbol, objPPI.entity2.id)
                dTemp[objPPI.entity2.type][t] = dTemp[objPPI.entity2.type].get(t, []) + [objPPI]
            for objPPI in a2:
                t = (objPPI.entity1.symbol, objPPI.entity1.id)
                dTemp[objPPI.entity1.type][t] = dTemp[objPPI.entity1.type].get(t, []) + [objPPI]
            # sort according to symbol
            dPPIHC = collections.OrderedDict(sorted(dTemp['HC'].items()))
            dPPI1 = collections.OrderedDict(sorted(dTemp['1'].items()))
            dPPI2 = collections.OrderedDict(sorted(dTemp['2'].items()))
            dPPI3 = collections.OrderedDict(sorted(dTemp['3'].items()))
            return dPPIHC, dPPI1, dPPI2, dPPI3
        
        # interactions
        if context["type"] == "TcoF":
            aPPI = tftcof.objects.filter(entity2=obj.id).distinct().order_by('entity1__symbol')
            dPPI = adjust_data_tf(aPPI)
            context["ppitf"] = dPPI
            aPPI2 = tcoftcof.objects.filter(entity1=obj.id).distinct().order_by('entity2__symbol')
            aPPI3 = tcoftcof.objects.filter(entity2=obj.id).distinct().order_by('entity1__symbol')
            dPPI2, dPPI3, dPPI4, dPPI5 = adjust_data2(aPPI2, aPPI3)
            context["ppitcofhc"] = dPPI2
            context["ppitcof1"]  = dPPI3
            context["ppitcof2"]  = dPPI4
            context["ppitcof3"]  = dPPI5

        elif context["type"] == "TF":
            aPPI1 = tftf.objects.filter(entity1=obj.id).distinct().order_by('entity2__symbol')
            dPPI1 = adjust_data(aPPI1)
            context["ppitf"] = dPPI1
            aPPI2 = tftcof.objects.filter(entity1=obj.id, entity2__type="HC").distinct().order_by('entity2__symbol')
            dPPI2 = adjust_data(aPPI2)
            context["ppitcofhc"] = dPPI2
            aPPI3 = tftcof.objects.filter(entity1=obj.id, entity2__type="1").distinct().order_by('entity2__symbol')
            dPPI3 = adjust_data(aPPI3)
            context["ppitcof1"] = dPPI3
            aPPI4 = tftcof.objects.filter(entity1=obj.id, entity2__type="2").distinct().order_by('entity2__symbol')
            dPPI4 = adjust_data(aPPI4)
            context["ppitcof2"] = dPPI4
            aPPI5 = tftcof.objects.filter(entity1=obj.id, entity2__type="3").distinct().order_by('entity2__symbol')
            dPPI5 = adjust_data(aPPI5)
            context["ppitcof3"] = dPPI5
            aPPI6 = tfother.objects.filter(entity1=obj.id).distinct().order_by('entity2__symbol')
            dPPI6 = adjust_data(aPPI6)
            context["ppiother"] = dPPI6
        else:
            aPPI = tfother.objects.filter(entity2=obj.id).distinct().order_by('entity1__symbol')
            dPPI = adjust_data_tf(aPPI)
            context["ppitf"] = dPPI

        #return HttpResponse(context)
        return render(request, "tcof/gene.html", context)

## Non view methods
def create_data(aList, ppi='active'):
    data = []
    for e in aList:
        if e.type == "TF":
            url_type = "TF"
        elif e.type == "-":
            url_type = "other"
        else:
            url_type = "TcoF:<small> class %s</small>" %e.type

        aTemp = [
            '<a title="Open gene in TcoF-DB" href="%s/gene/%s"><i class="fa fa-arrow-circle-right"></i></a>' %(_APP_LINK_PREFIX, e.symbol),
            '<a title="Open gene in TcoF-DB" href="%s/gene/%s">%s</a>' %(_APP_LINK_PREFIX, e.symbol, e.symbol),
            '<a title="Open at NCBI Gene" href="https://www.ncbi.nlm.nih.gov/gene/%s">%s</a>' %(e.id, e.id),
            '<a title="Open at NCBI Gene" href="https://www.ncbi.nlm.nih.gov/gene/%s">%s</a>' %(e.id, e.name),
            e.species,
            # ppi=active will switch to "Interactions" tab
            '<a title="Open gene in TcoF-DB" href="%s/gene/%s?ppi=%s">%s</a>' % (_APP_LINK_PREFIX, e.symbol, ppi, e.tcount),
            url_type
            ]
        a = [str(s) for s in aTemp]
        data.append(a)
    return data

