# TcoF-DB :: DJANGO application

## Installation
- Clone the repo:

```bash
git clone git@gitlab.com:s-schmeier/tcof.git
```

- Add `tcof` in the main project `settings.py `:

```python
# settings.py
INSTALLED_APPS = (
        ...,
        'tcof',
)
```

- Add `tcof` to the main projects `urls.py`:

```python
# urls.py
urlpatterns = [
       ...,
       url(r'^apps/tcof/', include('tcof.urls', namespace="tcof")),
]
```

- Update django database:

```bash
python manage.py makemigrations tcof
python manage.py migrate
```

- Load data into db, via json or postgres import

```bash
# json
time python manage.py loaddata tcof/data/entity.json
time python manage.py loaddata tcof/data/go.json 
time python manage.py loaddata tcof/data/uniprot.json 
time python manage.py loaddata tcof/data/tf_tf.json 
time python manage.py loaddata tcof/data/tf_tcof.json 
time python manage.py loaddata tcof/data/tf_other.json 
time python manage.py loaddata tcof/data/tcof_tcof.json 
time python manage.py loaddata tcof/data/c6_entities.json 
time python manage.py loaddata tcof/data/c7_entities.json 
time python manage.py loaddata tcof/data/c6.json 
time python manage.py loaddata tcof/data/c7.json 
time python manage.py loaddata tcof/data/kegg.json
time python manage.py loaddata tcof/data/reactome.json 
time python manage.py loaddata tcof/data/diseases.json 
```

```bash
# postgres
pg_restore --verbose --clean --no-acl --no-owner -h localhost -U django -d django tcof/data/psql-tcof-data.dump
```

- Collect static data

```bash
python manage.py collectstatic
```

## To delete data from tcof database do:

```bash
# open python shell
python manage.py shell
from tcof.models import Entity, Uniprot, Go, tftf, tftcof, tfother, c6, c7, tcoftcof, anno_c6, anno_c7, anno_kegg, anno_reactome, anno_disease
# delete table data
tfother.objects.all().delete()
tftcof.objects.all().delete()
tftf.objects.all().delete()
tcoftcof.objects.all().delete()
Go.objects.all().delete()
Uniprot.objects.all().delete()
Entity.objects.all().delete()
c6.objects.all().delete()
c7.objects.all().delete()
anno_c7.objects.all().delete()
anno_c6.objects.all().delete()
anno_kegg.objects.all().delete()
anno_reactome.objects.all().delete()
anno_disease.objects.all().delete()
```

