from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Entity(models.Model):
    symbol = models.CharField(max_length=20,
                              blank=False,
                              db_index=True)
    name = models.CharField(max_length=150,
                            db_index=True)
    alt = models.CharField(max_length=150,
                           db_index=True)
    species = models.CharField(max_length=5,
                               db_index=True)
    type = models.CharField(max_length=2,
                            db_index=True)


class Go(models.Model):
    entity = models.ForeignKey(Entity)
    goid = models.CharField(max_length=10)
    goterm = models.CharField(max_length=200)
    gotype = models.CharField(max_length=10,
                              db_index=True)
    goevidence = models.CharField(max_length=3)
    pmid = models.CharField(max_length=2300)
    kind = models.CharField(max_length=2,
                             db_index=True)

class Uniprot(models.Model):
    entity = models.ForeignKey(Entity)
    upacc = models.CharField(max_length=10,
                             db_index=True)
    upid = models.CharField(max_length=16,
                            db_index=True)
    kind = models.CharField(max_length=10,
                              db_index=True)
    name = models.CharField(max_length=800) #db_index too long
    seq = models.TextField()

class tftf(models.Model):
    entity1 = models.ForeignKey(Entity, related_name = 'tftf_tf1') # TFs
    entity2 = models.ForeignKey(Entity, related_name = 'tftf_tf2') # TFs
    pmid = models.CharField(max_length=8)
    dbid = models.CharField(max_length=400)
    exp = models.CharField(max_length=25)
    interaction = models.CharField(max_length=100)    
    db = models.CharField(max_length=8)

class tftcof(models.Model):
    entity1 = models.ForeignKey(Entity, related_name = 'tftcof_tf') # TFs
    entity2 = models.ForeignKey(Entity, related_name = 'tftcof_tcof') # non-TFs
    pmid = models.CharField(max_length=8)
    dbid = models.CharField(max_length=600)
    exp = models.CharField(max_length=25)
    interaction = models.CharField(max_length=100)
    db = models.CharField(max_length=8)

class tfother(models.Model):
    entity1 = models.ForeignKey(Entity, related_name = 'tfother_tf') # TFs
    entity2 = models.ForeignKey(Entity, related_name = 'tfother_protein') # non-TFs
    pmid = models.CharField(max_length=8)
    dbid = models.CharField(max_length=1200)
    exp = models.CharField(max_length=25)
    interaction = models.CharField(max_length=100)
    db = models.CharField(max_length=8)

class tcoftcof(models.Model):
    entity1 = models.ForeignKey(Entity, related_name = 'tcoftcof_tcof1') # Tcof
    entity2 = models.ForeignKey(Entity, related_name = 'tcoftcof_tcof2') # Tcof
    pmid = models.CharField(max_length=8)
    dbid = models.CharField(max_length=2400)
    exp = models.CharField(max_length=25)
    interaction = models.CharField(max_length=100)
    db = models.CharField(max_length=8)
    
class anno_c6(models.Model):
    entity = models.ForeignKey(Entity)
    idx = models.CharField(max_length=40, db_index=True)
    term = models.CharField(max_length=200)

class anno_c7(models.Model):
    entity = models.ForeignKey(Entity)
    idx = models.CharField(max_length=100, db_index=True)
    term = models.CharField(max_length=300)

class c7(models.Model):
    idx = models.CharField(max_length=100, db_index=True)
    term = models.CharField(max_length=300)
    count_TF = models.IntegerField()
    count_HC = models.IntegerField()
    count_1 = models.IntegerField()
    count_2 = models.IntegerField()
    count_3 = models.IntegerField()

class c6(models.Model):
    idx = models.CharField(max_length=40, db_index=True)
    term = models.CharField(max_length=200)
    count_TF = models.IntegerField()
    count_HC = models.IntegerField()
    count_1 = models.IntegerField()
    count_2 = models.IntegerField()
    count_3 = models.IntegerField()

class anno_kegg(models.Model):
    entity = models.ForeignKey(Entity)
    idx = models.CharField(max_length=20, db_index=True)
    term = models.CharField(max_length=100)

class anno_reactome(models.Model):
    entity = models.ForeignKey(Entity)
    idx = models.CharField(max_length=20, db_index=True)
    term = models.CharField(max_length=200)

class anno_disease(models.Model):
    entity = models.ForeignKey(Entity)
    idx = models.CharField(max_length=20, db_index=True)
    term = models.CharField(max_length=200)
    source = models.CharField(max_length=20)

